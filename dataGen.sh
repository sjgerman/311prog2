#!/bin/bash
# Census 2010 Population Data Sort Script
# By: Sean German <sjgerman@gmail.com>
# For: CSCI 311 / Project 2 @ Csu Chico

echo "Processing started, go get a cup of coffee!"

echo "Run 1" >> timingSortData 

echo "Census 2010 Population Alabama - Alabama" >> timingSortData
./csort CENSUS2010POP-Alabama-Alabama.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - California" >> timingSortData
./csort CENSUS2010POP-Alabama-California.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - Idaho" >> timingSortData
./csort CENSUS2010POP-Alabama-Idaho.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - Iowa" >> timingSortData
./csort CENSUS2010POP-Alabama-Iowa.csv | grep -B 1 CPU >> timingSortData

echo "Census 2010 Population Alabama - Missouri" >> timingSortData
./csort CENSUS2010POP-Alabama-Missouri.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population" >> timingSortData
./csort CENSUS2010POP.csv | grep -B 1 CPU >> timingSortData 

echo "End of Run 1, Proceeding to Run 2..." >> timingSortData


echo "Run 2" >> timingSortData 

echo "Census 2010 Population Alabama - Alabama" >> timingSortData
./csort CENSUS2010POP-Alabama-Alabama.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - California" >> timingSortData
./csort CENSUS2010POP-Alabama-California.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - Idaho" >> timingSortData
./csort CENSUS2010POP-Alabama-Idaho.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - Iowa" >> timingSortData
./csort CENSUS2010POP-Alabama-Iowa.csv | grep -B 1 CPU >> timingSortData

echo "Census 2010 Population Alabama - Missouri" >> timingSortData
./csort CENSUS2010POP-Alabama-Missouri.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population" >> timingSortData
./csort CENSUS2010POP.csv | grep -B 1 CPU >> timingSortData 

echo "End of Run 2, Proceeding to Run 3..." >> timingSortData


echo "Run 3" >> timingSortData 

echo "Census 2010 Population Alabama - Alabama" >> timingSortData
./csort CENSUS2010POP-Alabama-Alabama.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - California" >> timingSortData
./csort CENSUS2010POP-Alabama-California.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - Idaho" >> timingSortData
./csort CENSUS2010POP-Alabama-Idaho.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - Iowa" >> timingSortData
./csort CENSUS2010POP-Alabama-Iowa.csv | grep -B 1 CPU >> timingSortData

echo "Census 2010 Population Alabama - Missouri" >> timingSortData
./csort CENSUS2010POP-Alabama-Missouri.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population" >> timingSortData
./csort CENSUS2010POP.csv | grep -B 1 CPU >> timingSortData 

echo "End of Run 3, Proceeding to Run 4..." >> timingSortData


echo "Run 4" >> timingSortData 

echo "Census 2010 Population Alabama - Alabama" >> timingSortData
./csort CENSUS2010POP-Alabama-Alabama.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - California" >> timingSortData
./csort CENSUS2010POP-Alabama-California.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - Idaho" >> timingSortData
./csort CENSUS2010POP-Alabama-Idaho.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - Iowa" >> timingSortData
./csort CENSUS2010POP-Alabama-Iowa.csv | grep -B 1 CPU >> timingSortData

echo "Census 2010 Population Alabama - Missouri" >> timingSortData
./csort CENSUS2010POP-Alabama-Missouri.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population" >> timingSortData
./csort CENSUS2010POP.csv | grep -B 1 CPU >> timingSortData 

echo "End of Run 4, Proceeding to Run 5..." >> timingSortData


echo "Run 5" >> timingSortData 

echo "Census 2010 Population Alabama - Alabama" >> timingSortData
./csort CENSUS2010POP-Alabama-Alabama.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - California" >> timingSortData
./csort CENSUS2010POP-Alabama-California.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - Idaho" >> timingSortData
./csort CENSUS2010POP-Alabama-Idaho.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population Alabama - Iowa" >> timingSortData
./csort CENSUS2010POP-Alabama-Iowa.csv | grep -B 1 CPU >> timingSortData

echo "Census 2010 Population Alabama - Missouri" >> timingSortData
./csort CENSUS2010POP-Alabama-Missouri.csv | grep -B 1 CPU >> timingSortData 

echo "Census 2010 Population" >> timingSortData
./csort CENSUS2010POP.csv | grep -B 1 CPU >> timingSortData 

echo "End of Run 5." >> timingSortData

echo "Processing Complete, time to work!"