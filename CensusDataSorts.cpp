/**
 * @file CensusData.cpp   Represents census population data.
 * 
 * @brief
 *    This file contains all of the sorting functions and their helpers.
 * 
 * @author Sean German
 * @date 9/29/13
 */


 

#include "CensusData.h"
#include <string>
using namespace std;

/*
   static const int POPULATION = 0;       // type of sort
   static const int NAME = 1;             // This is type
    
    INSERTION-SORT.A/
1 for j = 2 to A.length
2 {
3   key = A[j]   
    //  Insert A[j] into the sorted sequence A[1..j-1]
4   i = j - 1
5   while i >0 and A[i] > key
6   {   
7       A[i + 1] = A[i]
8       i = (i - 1)
9   }
10  A[i + 1] = key

*/
void CensusData::insertionSort(int type)  
{
    if (type == 0)
    {
        Record* temp;
        int i = 0;
        for (int j = 1; j < (int)data.size(); j++)
        {
            temp = data[j];
            i = j;
            while (i > 0 && data[(i - 1)]->population > temp->population)
            {
                data[i] = data[i - 1];
                i--;
            }
            data[i] = temp;
        }   
    }

    else if (type == 1)
    {
        Record* temp;
        int i = 0;
        for (int j = 1; j < (int)data.size(); j++)
        {
            temp = data[j];
            i = j;
            while (i > 0 && (*data[(i - 1)]->city) > (*temp->city))
            {
                data[i] = data[i - 1];
                i--; 
           }
            data[i] = temp;
        }   
    }
}

/*
   static const int POPULATION = 0;       // type of sort
   static const int NAME = 1;             // This is type

   Merge-Sort(A, p, r)
   if p < r
    q = (p + r)/2 rounded down
    Merge-Sort(A, p, q)
    Merge-Sort(A, q + 1, r)
    Merge(A, p, q, r)

    splitterstuff(data, 0, (data.size()-1), type);
    std::vector<Record*> & data, int left, int right, int type) {}
    void whatever ^^^^^^^

*/
void CensusData::mergeSort(int type) 
{
    mergeHelper(data, 0, (data.size() - 1), type);
}


void CensusData::mergeHelper(std::vector<Record*> & data, int left, int right, int type)
{
    if (left < right)
    {
        int middle = ((left + right)/2);
        mergeHelper(data, left, middle, type);
        mergeHelper(data, (middle + 1), right, type);
        merge(data, left, middle, right, type);
    }
}

/*
    static const int POPULATION = 0;    // type of sort
    static const int NAME = 1;          // this is the type

    Merge(A, p, q, r)
    num1 = q - p + 1
    num2 = r - q
    let L[1..num1 + 1] and R[1..num2 + 1] be new arrays
    for i = 1 to num1
        L[i] = A[p + i - 1]
    for j = 1 to num2
        R[j] = A[q + j]
    L[num1 + 1] = infinity
    R[num2 + 1] = infinity
    i = 1
    j = 1
    for k = p to r
        if L[i] <= R[j]
            A[k] = L[i]
            i = i + 1
        else A[k] = R[j]
            j = j + 1
*/
void CensusData::merge(std::vector<Record*> & data, int left, int middle, int right, int type)
{
    int num1 = (middle - left + 1);
    int num2 = (right - middle);
    Record* temp;
    Record* left[(num1 + 1)];
    Record* right[(num2 + 1)];
    for (int i = 0; i < num1; i++)
    {
        left[i] = data[left + i - 1];
    }

    for (int j = 0; j < num2; j++)
    {
        right[j] = data[middle + j];
    }
    left[num1] = temp;
    right[num2] = temp;

    if (type == 0)
    {
        int i = 0;
        int j = 0;

        for (int k = left; k <= right; k++)
        {
            if (left[i]->population) <= (right[j]->population))
            {
                data[k] = left[i];
                i++;
            }
            else
            {
                data[k] = right[j]; 
                j++;  
            }
        }
    } 

    if (type == 1)
    {
        int i = 0;
        int j = 0;
        for (int k = left; k <= right; k++)
        {
            if (*left[i]->city) <= (*right[j]->city))
            {
                data[k] = left[i];
                i++;
            }
            else
            {
                data[k] = right[j]; 
                j++;  
            }
        }
    } 
}


/*

//delete[] right;
//delete[] left;
//delete[] temp;

   static const int POPULATION = 0;       // type of sort
   static const int NAME = 1;             // This is type

    if p < r
        q = Partition(A, p, r)
        Quicksort(A, p, q - 1)
        Quicksort(A, q + 1, r)
*/
void CensusData::quickSort(int type) 
{
    if (type == 0)
    {

    }

    else if (type == 1)
    {
    
    }
}

/*
   static const int POPULATION = 0;       // type of sort
   static const int NAME = 1;             // This is type

    x = A[r]
    i = p - 1
    for j = p to r - 1
        if A[j] <= x
            i = i + 1
            swap A[i] with A[j]
    swap A[i + 1] with A[r]
    return i + 1
*/
void CensusData::quickHelper(int type)
{


}
